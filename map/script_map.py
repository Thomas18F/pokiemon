import json
from xml.dom import minidom


def get_valeur(numero):
    if numero in ["155", "203", "204", "254", "271", "272", "273", "308", "321", "323", "358", "371", "372", "373", "403", "405", "452", "453", "454", "455", "490", "491", "492", "502", "503", "504", "505", "540", "541", "542", "552", "553", "554", "555", "590", "591", "633", "634", "635", "636", "637", "638", "683", "684", "685", "686", "687", "688", "764", "765", "2215", "2216", "2217", "2218", "2219", "2265", "2266", "2267", "2268", "2272", "2311", "2312", "2313", "2314", "2315", "2316", "2317", "2318", "2322", "2361", "2362", "2363", "2364", "2365", "2367", "2368", "2372", "2411", "2413", "2414"]:
        return {"type": "mur"}
    if numero in ["25", "26", "53", "75", "76", "101", "102", "103", "105", "124", "125", "126", "153", "154", "174", "175", "176", "224", "225", "226", "262", "304", "307", "354", "357", "404", "412", "413", "440", "441", "589", "592", "603", "755", "2165", "2166", "2167", "2168", "2169", "2261", "2262", "2263", "2264"]:
        return {"type": "libre"}
    if numero == "104":
        return {"type": "herbe", "id_zone": None}
    if numero in ["32", "127", "128", "129", "130", "131", "132", "177", "178", "179", "180", "181", "182", "227", "228", "229", "230", "231", "232"]:
        return {"type": "eau"}
    if numero in ["274", "275"]:
        return {"type": "bord", "direction": "bas"}
    if numero in ["276", "277"]:
        return {"type": "bord", "direction": "droite"}
    if numero in ["201", "816"]:
        return {"type": "panneau", "id_texte": None}
    if numero in ["2366", "2412"]:
        return {"type": "teleportation", "carte": None, "vers_x": None, "vers_y": None}
    return numero


def suppression_hitbox(nom):
    with open(nom + ".json", "r") as fichier:
        carte_donnees_json_complet = json.load(fichier)
        carte_donnees_json_complet["hitbox"] = []

        with open(nom + ".json", "w") as fichier_ecriture:
            json.dump(carte_donnees_json_complet, fichier_ecriture, indent=4)


#suppression_hitbox("inazuma")


def faire_hitbox(nom):
    fichier_xml = minidom.parse(nom + ".tmx")

    donnees = fichier_xml.getElementsByTagName("data")
    matrice = donnees[0].firstChild.data

    matrice_parsee = [ligne.split(",")[:-1] for ligne in matrice.split("\n")[1:-2]]
    derniere_ligne = matrice.split("\n")[-2]
    matrice_parsee.append(derniere_ligne.split(","))

    nb_lignes = len(matrice_parsee)
    nb_colonnes = len(matrice_parsee[0])

    print("MATRICE XML")
    print("x :", nb_colonnes)
    print("y :", nb_lignes)
    [print(matrice_parsee_ligne) for matrice_parsee_ligne in matrice_parsee]

    with open(nom + ".json", "r") as fichier:
        carte_donnees_json_complet = json.load(fichier)
        carte_donnees_json = carte_donnees_json_complet["hitbox"]

        print("MATRICE JSON")
        print("x :", len(carte_donnees_json))
        print("y(0) :", len(carte_donnees_json[0] if len(carte_donnees_json) > 0 else "sans"))

        for x in range(0, nb_colonnes):
            for y in range(0, nb_lignes):
                if len(carte_donnees_json) >= x + 1:
                    if len(carte_donnees_json[x]) >= y + 1:
                        print(x, y, carte_donnees_json[x][y], matrice_parsee[y][x])
                        if carte_donnees_json[x][y] is None or type(carte_donnees_json[x][y]) == str:
                            carte_donnees_json[x][y] = get_valeur(matrice_parsee[y][x])
                    else:
                        print(matrice_parsee[y])
                        valeur_generee = get_valeur(matrice_parsee[y][x])
                        carte_donnees_json[x].append(valeur_generee)
                else:
                    print("ajout")
                    carte_donnees_json.append([get_valeur(matrice_parsee[y][x])])

        with open(nom + ".json", "w") as fichier_ecriture:
            json.dump(carte_donnees_json_complet, fichier_ecriture, indent=4)


faire_hitbox("maison_depart")


def print_hitbox(nom):
    with open(nom + ".json", "r") as fichier:
        carte_donnees_json_complet = json.load(fichier)
        carte_donnees_json = carte_donnees_json_complet["hitbox"]

        nb_lignes = len(carte_donnees_json[0])
        nb_colonnes = len(carte_donnees_json)

        for y in range(0, nb_lignes):
            non_assigne = []
            for x in range(0, nb_colonnes):
                if carte_donnees_json[x][y] is None or type(carte_donnees_json[x][y]) == str:
                    print("n", end="")
                    non_assigne.append(carte_donnees_json[x][y])
                else:
                    if carte_donnees_json[x][y]["type"] == "mur":
                        print("m", end="")
                    elif carte_donnees_json[x][y]["type"] == "libre":
                        print("l", end="")
                    elif carte_donnees_json[x][y]["type"] == "herbe":
                        print("h", end="")
                    elif carte_donnees_json[x][y]["type"] == "eau":
                        print("e", end="")
                    elif carte_donnees_json[x][y]["type"] == "bord":
                        print("b", end="")
                    elif carte_donnees_json[x][y]["type"] == "objet":
                        print("o", end="")
                    elif carte_donnees_json[x][y]["type"] == "panneau":
                        print("p", end="")
                    elif carte_donnees_json[x][y]["type"] == "teleportation":
                        print("t", end="")
            print("  -> " + ",".join(non_assigne), end="")
            print("")


#print_hitbox("route_1")


def ecrire(nom, cases, donnees):
    with open(nom + ".json", "r") as fichier:
        carte_donnees_json_complet = json.load(fichier)
        carte_donnees_json = carte_donnees_json_complet["hitbox"]

        print("POUR " + str(nom) + " :")
        for case in cases:
            print("EN (" + str(case[0]) + ", " + str(case[1]) + ") : change " + str(carte_donnees_json[case[0]][case[1]]) + " par " + str(donnees))
            carte_donnees_json[case[0]][case[1]] = donnees

        with open(nom + ".json", "w") as fichier_ecriture:
            json.dump(carte_donnees_json_complet, fichier_ecriture, indent=4)


dico_tp = {
    "type": "teleportation",
    "carte": "inazuma",
    "vers_x": 15,
    "vers_y": 2
}
dico_libre = {"type": "libre_mais", "bas": False}
#ecrire("carrefour_inazuma", [[15, 48 + x] for x in range(0, 11)], {"type": "libre"})
#ecrire("carrefour_inazuma", [[15, 58]], dico_tp)
