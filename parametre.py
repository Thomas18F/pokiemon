import pygame
from pygame import Rect


class Ecran:
    VERSION = "2.6.0"

    LARGEUR = 1024
    HAUTEUR = 768
    FPS = 60
    TAILLE_TUILE = 64

    PERSONNAGE_TAILLE_X = TAILLE_TUILE
    PERSONNAGE_TAILLE_Y = 2 * TAILLE_TUILE - 20

    POSITION_TEXTE_X = LARGEUR // 2
    POSITION_TEXTE_Y = HAUTEUR - 100
    CADRE_TEXTE_LARGEUR = LARGEUR - 60
    CADRE_TEXTE_HAUTEUR = 70

    CADRE_TEXTE: Rect = Rect(
        POSITION_TEXTE_X - (CADRE_TEXTE_LARGEUR // 2),
        POSITION_TEXTE_Y - (CADRE_TEXTE_HAUTEUR // 2),
        CADRE_TEXTE_LARGEUR,
        CADRE_TEXTE_HAUTEUR,
    )

    TEMPS_MAXIMUM_DIALOGUE = 2500
    TEMPS_DEPLACEMENT = 130

    DECALAGE_SPRITE_JOUEUR_HAUT = ((PERSONNAGE_TAILLE_Y - TAILLE_TUILE) // 2) - TAILLE_TUILE - 2


class Couleur:
    BLANC = (255, 255, 255)
    NOIR = (0, 0, 0)
    ROSE = (255, 174, 201)
    ROUGE = (255, 0, 0)
    VERT = (0, 255, 0)
    BLEU = (0, 0, 255)
    JAUNE = (255, 255, 0)
    VERT_FONCE = (20, 140, 60)


class Touche:
    DEPLACEMENT_HAUT = pygame.K_UP
    DEPLACEMENT_BAS = pygame.K_DOWN
    DEPLACEMENT_GAUCHE = pygame.K_LEFT
    DEPLACEMENT_DROITE = pygame.K_RIGHT

    ACTION = pygame.K_RETURN

    SAC = pygame.K_s

    SON = pygame.K_e
    SON_PLUS = pygame.K_z
    SON_MOINS = pygame.K_a


class Musique:
    MUSIQUE_MENU = "menu.mp3"

    TEMPS_DISPARITION = 2000
    VOLUME_DEFAUT = 0.5


class Params:
    ANIMATION_COMBAT = True
    RENCONTRE_ALEATOIRE = True
    MUSIQUE = True
    SANS_COLLISION = False
