from typing import Dict

import pygame


class GestionTouche:
    DELAI_MINIMUM = 12

    def __init__(self):
        self.touches = {}

    def ajout_supervision_touche(self, touche):
        self.touches[touche]: Dict = EtatTouche()

    def maj(self):
        touches_pressees = pygame.key.get_pressed()
        for touche in self.touches:
            if touches_pressees[touche]:
                if self.touches[touche].est_pressee:
                    self.touches[touche].est_pressee = False
                    self.touches[touche].iterations_apres_clic = 1
                elif self.touches[touche].iterations_apres_clic == 0 or self.touches[touche].iterations_apres_clic >= GestionTouche.DELAI_MINIMUM:
                    self.touches[touche].est_pressee = True
                    self.touches[touche].iterations_apres_clic = 0
                else:
                    self.touches[touche].iterations_apres_clic += 1
            else:
                self.touches[touche].est_pressee = False
                self.touches[touche].iterations_apres_clic = 0

    def est_pressee(self, touche):
        return self.touches[touche].est_pressee and self.touches[touche].actif

    def bloque(self, touche):
        self.touches[touche].actif = False

    def libere(self, touche):
        self.touches[touche].actif = True


class EtatTouche:
    def __init__(self):
        self.est_pressee: bool = False
        self.iterations_apres_clic: int = 0
        self.actif: bool = True
