import pygame
from pygame.surface import Surface

from EC.parametre_combat import EcranCombat
from EC.texte import Texte
from classe.pokemon import Pokemon
from parametre import Couleur


class ObjetVisuel:
    @staticmethod
    def affiche_bandeau_pokemon(pokemon: Pokemon, surface: Surface):
        BarreDeVie.affichage(
            pokemon.PV,
            surface,
            pokemon.image.rect.x, pokemon.image.rect.y - EcranCombat.DISTANCE_BARRE_DE_VIE_AFFICHAGE_PV_Y,
            pokemon.PV_actuel,
        )
        Texte.affiche_texte(
            surface,
            pokemon.nom + " niv. " + str(pokemon.niveau),
            30,
            pokemon.image.rect.x, pokemon.image.rect.y - EcranCombat.DISTANCE_POKEMON_NOM_POKEMON_Y,
            Couleur.NOIR,
        )
        surface.blit(pokemon.surface, pokemon.image.rect)


class BarreDeVie:
    HAUTEUR_BARRE = 10
    LARGEUR_TRAIT_EXTERIEUR = 2

    @staticmethod
    def affichage(pv_max, surface_affichage, x, y, pv_actuel):
        BarreDeVie.affichage_barre(pv_actuel, pv_max, x, y, surface_affichage)
        BarreDeVie.affichage_texte(pv_actuel, pv_max, x, y, surface_affichage)

    @staticmethod
    def affichage_barre(pv_actuel, pv_max, x, y, surface_affichage):
        if pv_actuel < 0:
            pv_actuel = 0

        couleur = Couleur.VERT
        if pv_actuel/pv_max < 1/10:
            couleur = Couleur.ROUGE
        elif pv_actuel/pv_max < 1/2:
            couleur = Couleur.JAUNE

        longueur_barre = pv_max
        fill = pv_actuel
        outline_rect = pygame.Rect(x, y, longueur_barre, BarreDeVie.HAUTEUR_BARRE)
        fill_rect = pygame.Rect(x, y, fill, BarreDeVie.HAUTEUR_BARRE)
        pygame.draw.rect(surface_affichage, couleur, fill_rect)
        pygame.draw.rect(surface_affichage, Couleur.BLANC, outline_rect, BarreDeVie.LARGEUR_TRAIT_EXTERIEUR)

    @staticmethod
    def affichage_texte(pv_actuel, pv_max, x, y, surface_affichage):
        Texte.affiche_texte(
            surface_affichage,
            str(pv_actuel) + "/" + str(pv_max),
            20,
            x, y - EcranCombat.DISTANCE_BARRE_DE_VIE_AFFICHAGE_PV_Y,
            Couleur.NOIR,
        )


