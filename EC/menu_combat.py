import pygame

from EC.parametre_combat import SelectionMenu, StatutMenu, EcranCombat, StatutSelection
from EC.texte import Texte
from classe.pokemon import Pokemon
from parametre import Couleur


class MenuCombat(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface((EcranCombat.LARGEUR, EcranCombat.HAUTEUR_MENU))
        self.rect = self.image.get_rect()
        self.r = pygame.Rect.copy(self.rect)
        self.rect.centerx = EcranCombat.LARGEUR / 2
        self.rect.bottom = EcranCombat.HAUTEUR_FOND

        self.x = self.image.get_rect().centerx
        self.y = self.image.get_rect().centery

        self.statut: StatutSelection = None

    def dessine_menu_principal(self):
        self.dessine_fond()
        Texte.affiche_texte_cadre(
            self.image,
            "ATTAQUE",
            EcranCombat.MENU_POLICE,
            EcranCombat.ATTAQUE_POS_X,
            EcranCombat.ATTAQUE_POS_Y,
            Couleur.NOIR,
            EcranCombat.CADRE_ATTAQUE,
        )

        Texte.affiche_texte_cadre(
            self.image,
            "SAC",
            EcranCombat.MENU_POLICE,
            EcranCombat.SAC_POS_X,
            EcranCombat.SAC_POS_Y,
            Couleur.NOIR,
            EcranCombat.CADRE_SAC,
        )

        Texte.affiche_texte_cadre(
            self.image,
            "FUITE",
            EcranCombat.MENU_POLICE,
            EcranCombat.FUITE_POS_X,
            EcranCombat.FUITE_POS_Y,
            Couleur.NOIR,
            EcranCombat.CADRE_FUITE,
        )

        Texte.affiche_texte_cadre(
            self.image,
            "POKEMON",
            EcranCombat.MENU_POLICE,
            EcranCombat.CHANGEMENT_POS_X,
            EcranCombat.CHANGEMENT_POS_Y,
            Couleur.NOIR,
            EcranCombat.CADRE_CHANGEMENT,
        )

    def dessine_menu_choix_attaque(self, pokemon: Pokemon):
        self.dessine_fond()
        nombre_attaques = len(pokemon.capacites)

        if nombre_attaques > 0:
            Texte.affiche_texte_cadre(
                self.image,
                pokemon.capacites[0].nom,
                EcranCombat.ATTAQUE_POLICE,
                EcranCombat.ATTAQUE_1_POS_X,
                EcranCombat.ATTAQUE_1_POS_Y,
                Couleur.NOIR,
                EcranCombat.CADRE_ATTAQUE_1,
            )

            if nombre_attaques > 1:
                Texte.affiche_texte_cadre(
                    self.image,
                    pokemon.capacites[1].nom,
                    EcranCombat.ATTAQUE_POLICE,
                    EcranCombat.ATTAQUE_2_POS_X,
                    EcranCombat.ATTAQUE_2_POS_Y,
                    Couleur.NOIR,
                    EcranCombat.CADRE_ATTAQUE_2,
                )

                if nombre_attaques > 2:
                    Texte.affiche_texte_cadre(
                        self.image,
                        pokemon.capacites[2].nom,
                        EcranCombat.ATTAQUE_POLICE,
                        EcranCombat.ATTAQUE_3_POS_X,
                        EcranCombat.ATTAQUE_3_POS_Y,
                        Couleur.NOIR,
                        EcranCombat.CADRE_ATTAQUE_3,
                    )

        Texte.affiche_texte_cadre(
            self.image,
            "Retour",
            EcranCombat.ATTAQUE_POLICE,
            EcranCombat.ATTAQUE_RETOUR_POS_X,
            EcranCombat.ATTAQUE_RETOUR_POS_Y,
            Couleur.NOIR,
            EcranCombat.CADRE_ATTAQUE_RETOUR,
        )

    def dessine_fond(self):
        self.image.fill(Couleur.BLANC)
        pygame.draw.rect(self.image, Couleur.NOIR, self.r, 2)

    def dessine_selection(self, pokemon: Pokemon):
        if self.statut.selection == SelectionMenu.PRINCIPAL:
            if self.statut.menu == StatutMenu.ATTAQUE:
                pygame.draw.rect(self.image, Couleur.ROUGE, EcranCombat.CADRE_ATTAQUE, 2)
                pygame.draw.rect(self.image, Couleur.NOIR, EcranCombat.CADRE_SAC, 2)
            elif self.statut.menu == StatutMenu.SAC:
                pygame.draw.rect(self.image, Couleur.ROUGE, EcranCombat.CADRE_SAC, 2)
                pygame.draw.rect(self.image, Couleur.NOIR, EcranCombat.CADRE_ATTAQUE, 2)
