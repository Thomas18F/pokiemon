from typing import List, Union, Dict

from classe.pokemon import Pokemon


class Opposant:
    def __init__(self, sauvage: bool, liste_pokemons: List[Pokemon], pre_combat: Union[str, None], musique: str, valeur: str):
        self.sauvage: bool = sauvage
        self.liste_pokemons: List[Pokemon] = liste_pokemons
        self.pre_combat: Union[str, None] = pre_combat
        self.musique: str = musique
        self.valeur: str = valeur

    def niveau_max(self):
        maximum = 1
        for pokemon in self.liste_pokemons:
            if pokemon.niveau > maximum:
                maximum = pokemon.niveau
        return maximum

    @staticmethod
    def pokemon_sauvage(pokemon: Pokemon):
        return Opposant(True, [pokemon], None, "sauvage", "sans")

    @staticmethod
    def dresseur(donnees: Dict):
        return Opposant(
            False,
            [Pokemon.from_mix(pokemon) for pokemon in donnees["pokemons"]],
            donnees["nom"] + " : " + donnees["pre_combat"],
            donnees.get("musique", "dresseur"),
            donnees["valeur"]
        )
