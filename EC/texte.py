from typing import Union

import pygame
from pygame import font, Rect
from pygame.surface import Surface

from parametre import Couleur, Ecran


class Texte:
    POLICE = font.match_font("pokemon")

    @staticmethod
    def affiche_texte(
        surface: Surface,
        texte: str,
        taille_police: int,
        x: Union[int, float],
        y: Union[int, float],
        couleur: Couleur,
        **params,
    ):
        police = font.Font(Texte.POLICE, taille_police)
        text_surface = police.render(texte, True, couleur)
        rectangle_texte = text_surface.get_rect()
        if params.get("centre", False):
            rectangle_texte.center = (x, y)
        else:
            rectangle_texte.midleft = (x, y)
        surface.blit(text_surface, rectangle_texte)

        return rectangle_texte

    @staticmethod
    def affiche_texte_cadre(
        surface: Surface,
        texte: str,
        taille_police: int,
        x: Union[int, float],
        y: Union[int, float],
        couleur_texte: Couleur,
        cadre: Rect,
        **params,
    ):
        Texte.affiche_texte(surface, texte, taille_police, x, y, couleur_texte, centre=True)
        pygame.draw.rect(
            surface,
            params.get("cadre_couleur", Couleur.NOIR),
            cadre,
            params.get("cadre_largeur", 2),
            params.get("arrondi", -1),
        )

    @staticmethod
    def affiche_dialogue(
        surface: Surface,
        texte: str,
        **params,
    ):
        image = pygame.Surface((Ecran.CADRE_TEXTE_LARGEUR, Ecran.CADRE_TEXTE_HAUTEUR))
        image.fill(Couleur.BLANC)
        surface.blit(
            image,
            (
                Ecran.POSITION_TEXTE_X - Ecran.CADRE_TEXTE_LARGEUR // 2,
                Ecran.POSITION_TEXTE_Y - Ecran.CADRE_TEXTE_HAUTEUR // 2,
            ),
        )
        Texte.affiche_texte_cadre(
            surface,
            texte,
            params.get("taille_police", 22),
            Ecran.POSITION_TEXTE_X,
            Ecran.POSITION_TEXTE_Y,
            params.get("couleur_texte", Couleur.NOIR),
            Ecran.CADRE_TEXTE,
            cadre_couleur=Couleur.NOIR,
            cadre_largeur=2,
            arrondi=-1,
        )
        pygame.display.flip()
