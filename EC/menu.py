from random import randint, random
from typing import Union

import pygame
from pygame.rect import Rect
from pygame.sprite import Sprite
from pygame.surface import Surface

from EC.controleur_action import ControleurAction
from EC.menu_combat import MenuCombat
from EC.opposant import Opposant
from EC.resultat_attaque import ResultatAttaque
from EC.texte import Texte
from EC.objets_visuel import ObjetVisuel
from EC.parametre_combat import EcranCombat, StatutSelectionAttaque, StatutSelection, SelectionMenu, StatutMenu
from EC.victoire_type import VictoireType
from carte import Tmap
from classe.attaque import Attaque, AttaqueCategorie
from classe.pokemon import Pokemon, PokemonCalculateur
from classe.type import Types
from gestion_touche import GestionTouche

from joueur import Joueur
from parametre import Couleur


def type_action(fonction_action):
    return fonction_action


class EC(Sprite):
    def __init__(self, joueur: Joueur):
        Sprite.__init__(self)

        self.image: Surface = Surface((EcranCombat.LARGEUR, EcranCombat.HAUTEUR_FOND))
        self.image.fill(Couleur.VERT_FONCE)

        self.rect: Rect = self.image.get_rect()
        self.r: Rect = Rect.copy(self.rect)
        pygame.draw.rect(self.image, Couleur.NOIR, self.r, 2)

        self.rect.centerx = EcranCombat.POSITION_X
        self.rect.centery = EcranCombat.POSITION_Y

        self.menu: MenuCombat = MenuCombat()
        self.joueur: Joueur = joueur

        self.en_combat: bool = False

        self.gestion_touche: GestionTouche = GestionTouche()
        self.gestion_touche.ajout_supervision_touche(pygame.K_LEFT)
        self.gestion_touche.ajout_supervision_touche(pygame.K_RIGHT)
        self.gestion_touche.ajout_supervision_touche(pygame.K_RETURN)

    def lance_combat(self, opposant: Opposant):

        self.en_combat = True
        self.vainqueur: VictoireType = VictoireType.NON_TERMINE

        self.pokemon_au_combat: Pokemon = self.joueur.pokemons[0]

        self.pokemon_au_combat.place(
            EcranCombat.POSITION_POKEMON_JOUEUR_X,
            EcranCombat.POSITION_POKEMON_JOUEUR_Y,
            tourne=True,
        )

        self.opposant: Opposant = opposant

        self.pokemon_adverse: Pokemon = self.opposant.liste_pokemons[0]
        self.pokemon_adverse.place(
            EcranCombat.POSITION_POKEMON_OPPOSANT_X,
            EcranCombat.POSITION_POKEMON_OPPOSANT_Y,
        )

        self.statut: StatutSelection = StatutSelection()
        self.menu.statut = self.statut

        self.controleur_action: ControleurAction = ControleurAction()
        self.derniere_action: int = 0

        self.menu.dessine_menu_principal()

    def maj(self):
        Sprite.update(self)
        self.gestion_touche.maj()

        self.dessine_combat()

        self.derniere_action -= 1

        action: Union[type_action, None] = self.controleur_action.get_action()
        if action is not None:
            action()

        else:
            if self.statut.selection == SelectionMenu.PRINCIPAL and self.derniere_action <= 0:
                if self.gestion_touche.est_pressee(pygame.K_LEFT):
                    self.statut.menu = StatutMenu.ATTAQUE
                    self.menu.dessine_selection(self.pokemon_au_combat)
                    self.derniere_action = 10

                if self.gestion_touche.est_pressee(pygame.K_RIGHT):
                    self.statut.menu = StatutMenu.SAC
                    self.menu.dessine_selection(self.pokemon_au_combat)
                    self.derniere_action = 10

                if self.gestion_touche.est_pressee(pygame.K_RETURN):
                    if self.statut.menu == StatutMenu.ATTAQUE:
                        self.statut.selection = SelectionMenu.ATTAQUE
                        self.derniere_action = 10
                        self.menu.dessine_menu_choix_attaque(self.pokemon_au_combat)

            if self.statut.selection == SelectionMenu.ATTAQUE and self.derniere_action <= 0:
                deplacement_selection = False
                if self.gestion_touche.est_pressee(pygame.K_LEFT):
                    pygame.draw.rect(self.menu.image, Couleur.ROUGE, EcranCombat.CADRE_ATTAQUE_1, 2)
                    pygame.draw.rect(self.menu.image, Couleur.NOIR, EcranCombat.CADRE_ATTAQUE_2, 2)
                    if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_2:
                        self.statut.attaque = StatutSelectionAttaque.ATTAQUE_1
                    self.derniere_action = 10
                    deplacement_selection = True

                if self.gestion_touche.est_pressee(pygame.K_RIGHT):
                    pygame.draw.rect(self.menu.image, Couleur.NOIR, EcranCombat.CADRE_ATTAQUE_1, 2)
                    pygame.draw.rect(self.menu.image, Couleur.ROUGE, EcranCombat.CADRE_ATTAQUE_2, 2)
                    if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_1 and len(self.pokemon_au_combat.capacites) >= 2:
                        self.statut.attaque = StatutSelectionAttaque.ATTAQUE_2
                    if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_2 and len(self.pokemon_au_combat.capacites) >= 3:
                        self.statut.attaque = StatutSelectionAttaque.ATTAQUE_3
                    self.derniere_action = 10
                    deplacement_selection = True

                if deplacement_selection:
                    pygame.draw.rect(
                        self.menu.image,
                        Couleur.ROUGE if self.statut.attaque == StatutSelectionAttaque.RETOUR else Couleur.NOIR,
                        EcranCombat.CADRE_ATTAQUE_1,
                        2,
                    )
                    pygame.draw.rect(
                        self.menu.image,
                        Couleur.ROUGE if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_1 else Couleur.NOIR,
                        EcranCombat.CADRE_ATTAQUE_1,
                        2,
                    )
                    if len(self.pokemon_au_combat.capacites) >= 2:
                        pygame.draw.rect(
                            self.menu.image,
                            Couleur.ROUGE if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_2 else Couleur.NOIR,
                            EcranCombat.CADRE_ATTAQUE_2,
                            2,
                        )
                        if len(self.pokemon_au_combat.capacites) >= 3:
                            pygame.draw.rect(
                                self.menu.image,
                                Couleur.ROUGE if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_3 else Couleur.NOIR,
                                EcranCombat.CADRE_ATTAQUE_3,
                                2,
                            )

                if self.gestion_touche.est_pressee(pygame.K_RETURN):
                    self.gestion_touche.bloque(pygame.K_RETURN)
                    self.menu.image.fill(Couleur.BLANC)
                    pygame.draw.rect(self.menu.image, Couleur.NOIR, self.menu.r, 2)

                    self.derniere_action = 10

                    indice_attaque = -1
                    if self.statut.attaque == StatutSelectionAttaque.ATTAQUE_1:
                        indice_attaque = 0
                    elif self.statut.attaque == StatutSelectionAttaque.ATTAQUE_2:
                        indice_attaque = 1
                    resultat = EC.calcul_degats(self.pokemon_au_combat, self.pokemon_adverse, indice_attaque)

                    self.pokemon_adverse.PV_actuel = self.pokemon_adverse.PV_actuel - resultat.degat

                    if self.pokemon_adverse.PV_actuel > 0:
                        Texte.affiche_texte(
                            self.menu.image,
                            "Vous avez infligé " + str(resultat.degat) + " points de dégâts !",
                            35,
                            120, self.menu.image.get_height() / 2,
                            Couleur.NOIR,
                        )

                        self.statut.selection = SelectionMenu.PRINCIPAL

                        self.controleur_action.ajoute_action(EcranCombat.TEMPS_ACTION_ADVERSE, self.action_adverse)

                    else:
                        Texte.affiche_texte(
                            self.menu.image,
                            "Vous avez infligé " + str(resultat.degat) + " points de dégats.",
                            35,
                            120, self.menu.image.get_height() / 2,
                            Couleur.NOIR,
                        )
                        Texte.affiche_texte(
                            self.menu.image,
                            self.pokemon_adverse.nom + " est KO !",
                            35,
                            230, (self.menu.image.get_height() / 2) + 30,
                            Couleur.NOIR,
                        )

                        self.controleur_action.ajoute_action(EcranCombat.TEMPS_GAIN_EXPERIENCE, self.gain_exp)

    @type_action
    def action_adverse(self):
        choix_attaque_IA: int = randint(0, len(self.pokemon_adverse.capacites) - 1)
        resultat = EC.calcul_degats(self.pokemon_adverse, self.pokemon_au_combat, choix_attaque_IA)
        self.pokemon_au_combat.PV_actuel = self.pokemon_au_combat.PV_actuel - resultat.degat

        if self.pokemon_au_combat.PV_actuel > 0:
            self.menu.dessine_fond()
            Texte.affiche_texte(
                self.menu.image,
                "Vous avez subi " + str(resultat.degat) + " points de dégâts !",
                35,
                120, self.menu.image.get_height() / 2,
                Couleur.NOIR,
            )

            self.controleur_action.ajoute_action(2000, self.fin_tour)

        else:
            self.menu.dessine_fond()
            Texte.affiche_texte(
                self.menu.image,
                "Vous avez subi " + str(resultat.degat) + " points de dégats.",
                35,
                120, self.menu.image.get_height() / 2,
                Couleur.NOIR,
            )
            Texte.affiche_texte(
                self.menu.image,
                self.pokemon_au_combat.nom + " est KO !",
                35,
                230, (self.menu.image.get_height() / 2) + 30,
                Couleur.NOIR,
            )

            self.joueur.carte = Tmap.avec_nom(self.joueur.carte_sauvegarde_nom, self.joueur.app, self.joueur)
            self.joueur.app.set_carte(self.joueur.carte)
            self.joueur.coordonnes = self.joueur.coordonnes_sauvegarde
            self.joueur.actualise_position_rectangle()
            self.joueur.soin_total()

            self.vainqueur = VictoireType.ORDINATEUR
            self.en_combat = False

            self.gestion_touche.libere(pygame.K_RETURN)

    @type_action
    def fin_tour(self):
        self.gestion_touche.libere(pygame.K_RETURN)

        for statut in self.pokemon_adverse.statuts:
            if statut == "vampigraine":
                degat_prevu = int(self.pokemon_adverse.PV_actuel / 8) if self.pokemon_adverse.PV_actuel >= 16 else 1
                self.pokemon_adverse.PV_actuel -= degat_prevu

                Texte.affiche_texte(
                    self.menu.image,
                    "Vampigraine inflige " + str(degat_prevu) + " points de dégats à " + self.pokemon_adverse.nom + ".",
                    35,
                    120, self.menu.image.get_height() / 2,
                    Couleur.NOIR,
                )
                if self.pokemon_adverse.PV_actuel <= 0:

                    Texte.affiche_texte(
                        self.menu.image,
                        self.pokemon_adverse.nom + " est KO !",
                        35,
                        230, (self.menu.image.get_height() / 2) + 30,
                        Couleur.NOIR,
                    )

                    self.controleur_action.ajoute_action_directe(self.gain_exp)
                self.pokemon_au_combat.PV_actuel = min(self.pokemon_au_combat.PV_actuel + int(degat_prevu/2), self.pokemon_au_combat.PV)

        if self.pokemon_adverse.PV_actuel > 0:
            self.controleur_action.ajoute_action_directe(self.affiche_menu)

    @type_action
    def affiche_menu(self):
        self.menu.dessine_menu_principal()

    @type_action
    def declare_victoire_joueur(self):
        self.vainqueur = VictoireType.JOUEUR
        if not self.opposant.sauvage:
            self.joueur.valeurs.ajoute_valeur(self.joueur.carte.nom, self.opposant.valeur)
        self.en_combat = False

        self.gestion_touche.libere(pygame.K_RETURN)

    @type_action
    def gain_exp(self):
        self.menu.dessine_fond()

        experience = EC.calcul_experience(self.pokemon_au_combat, self.pokemon_adverse)
        self.pokemon_au_combat.experience += experience

        Texte.affiche_texte(
            self.menu.image,
            self.pokemon_au_combat.nom + " a gagné " + str(experience) + " points d'expérience.",
            35,
            EcranCombat.LARGEUR / 2, EcranCombat.HAUTEUR_MENU / 2,
            Couleur.NOIR,
            centre=True
        )

        if EC.calcul_niveau(self.pokemon_au_combat) == self.pokemon_au_combat.niveau:
            self.controleur_action.ajoute_action(EcranCombat.TEMPS_GAIN_ARGENT, self.gain_argent)
        else:
            self.controleur_action.ajoute_action(EcranCombat.TEMPS_GAIN_NIVEAU, self.gain_niveau)

    @type_action
    def gain_niveau(self):
        self.menu.dessine_fond()

        nouveau_niveau = EC.calcul_niveau(self.pokemon_au_combat)
        ancien_stat = [
            self.pokemon_au_combat.PV,
            self.pokemon_au_combat.att,
            self.pokemon_au_combat.defense,
            self.pokemon_au_combat.att_spe,
            self.pokemon_au_combat.def_spe,
            self.pokemon_au_combat.vit,
        ]

        self.pokemon_au_combat.PV = PokemonCalculateur.calcul_pv(self.pokemon_au_combat.stat_pv, nouveau_niveau)
        self.pokemon_au_combat.att = PokemonCalculateur.calcul_stat(
            self.pokemon_au_combat.stat_att,
            nouveau_niveau,
        )
        self.pokemon_au_combat.defense = PokemonCalculateur.calcul_stat(
            self.pokemon_au_combat.stat_defense,
            nouveau_niveau,
        )
        self.pokemon_au_combat.att_spe = PokemonCalculateur.calcul_stat(
            self.pokemon_au_combat.stat_att_spe,
            nouveau_niveau,
        )
        self.pokemon_au_combat.def_spe = PokemonCalculateur.calcul_stat(
            self.pokemon_au_combat.stat_def_spe,
            nouveau_niveau,
        )
        self.pokemon_au_combat.vit = PokemonCalculateur.calcul_stat(
            self.pokemon_au_combat.stat_vit,
            nouveau_niveau,
        )

        texte_ligne_1: str = self.pokemon_au_combat.nom
        texte_ligne_1 += " a gagné " + str(nouveau_niveau - self.pokemon_au_combat.niveau)
        texte_ligne_1 += " niveau " if nouveau_niveau - self.pokemon_au_combat.niveau == 1 else " niveaux"
        texte_ligne_2: str = "("
        texte_ligne_2 += "PV +" + str(self.pokemon_au_combat.PV - ancien_stat[0]) + ", "
        texte_ligne_2 += "att +" + str(self.pokemon_au_combat.att - ancien_stat[1]) + ", "
        texte_ligne_2 += "def +" + str(self.pokemon_au_combat.defense - ancien_stat[2]) + ", "
        texte_ligne_2 += "att spe +" + str(self.pokemon_au_combat.att_spe - ancien_stat[3]) + ", "
        texte_ligne_2 += "def spe +" + str(self.pokemon_au_combat.def_spe - ancien_stat[4]) + ", "
        texte_ligne_2 += "vit +" + str(self.pokemon_au_combat.vit - ancien_stat[5])
        texte_ligne_2 += ")"

        self.pokemon_au_combat.niveau = nouveau_niveau
        self.pokemon_au_combat.PV_actuel += self.pokemon_au_combat.PV - ancien_stat[0]

        Texte.affiche_texte(
            self.menu.image,
            texte_ligne_1,
            35,
            EcranCombat.LARGEUR / 2, EcranCombat.HAUTEUR_MENU / 2 - 15,
            Couleur.NOIR,
            centre=True
        )
        Texte.affiche_texte(
            self.menu.image,
            texte_ligne_2,
            35,
            EcranCombat.LARGEUR / 2, EcranCombat.HAUTEUR_MENU / 2 + 15,
            Couleur.NOIR,
            centre=True
        )

        self.controleur_action.ajoute_action(EcranCombat.TEMPS_MONTEE_NIVEAU, self.gain_argent)

    @type_action
    def gain_argent(self):
        self.menu.dessine_fond()

        argent = EC.calcul_argent(self.opposant)
        self.joueur.argent += argent
        print("argent : " + str(self.joueur.argent))

        Texte.affiche_texte(
            self.menu.image,
            "Vous gagnez " + str(argent) + "€.",
            35,
            EcranCombat.LARGEUR / 2, EcranCombat.HAUTEUR_MENU / 2,
            Couleur.NOIR,
            centre=True
        )

        self.controleur_action.ajoute_action(EcranCombat.TEMPS_DECLARE_VICTOIRE, self.declare_victoire_joueur)

    def dessine_combat(self):
        self.image.fill(Couleur.VERT_FONCE)
        pygame.draw.rect(self.image, Couleur.NOIR, self.r, 2)

        if self.pokemon_au_combat.PV_actuel > 0:
            ObjetVisuel.affiche_bandeau_pokemon(self.pokemon_au_combat, self.image)
        if self.pokemon_adverse.PV_actuel > 0:
            ObjetVisuel.affiche_bandeau_pokemon(self.pokemon_adverse, self.image)
        self.image.blit(self.menu.image, self.menu.rect)

    @staticmethod
    def calcul_degats(pokemon_attaquant: Pokemon, pokemon_subissant: Pokemon, indice_attaque: int) -> ResultatAttaque:
        resultat: ResultatAttaque = ResultatAttaque()

        attaque: Attaque = pokemon_attaquant.capacites[indice_attaque]

        for effet in attaque.effets:
            if random() < effet["proba"] / 100:
                if effet["cible"] == "cible":
                    nom_effet = effet["effet"]
                    # TODO
                    """
                    image = pygame.image.load("EC\\img\\stat\\" + nom_effet + ".png").convert_alpha()
                    surface = Surface((EcranCombat.TAILLE_POKEMON_X, EcranCombat.TAILLE_POKEMON_Y))
                    surface.blit(image, (400, 400))
                    """
                    pokemon_subissant.statuts.append(nom_effet)

        resultat.attaque_categorie = attaque.categorie
        if attaque.categorie == AttaqueCategorie.PHYSIQUE:
            att = pokemon_attaquant.att
            defense = pokemon_subissant.defense
        elif attaque.categorie == AttaqueCategorie.SPECIAL:
            att = pokemon_attaquant.att_spe
            defense = pokemon_subissant.def_spe
        else:
            return resultat

        efficacite = Types.affinite_simple(attaque.type_att, pokemon_subissant.types[0])
        if len(pokemon_subissant.types) >= 2:
            efficacite *= Types.affinite_simple(attaque.type_att, pokemon_subissant.types[1])

        stab = 1.5 if pokemon_attaquant.types[0] is attaque.type_att else 1
        if len(pokemon_subissant.types) >= 3:
            stab *= 1.4 if pokemon_attaquant.types[2] is attaque.type_att else 1

        est_critique: bool = True if random() < 0.2 else False
        resultat.critique = est_critique
        critique: int = 1.5 if est_critique else 1

        aleatoire = (random() * 15 / 100) + 0.85

        coef_multiplicateur = efficacite * stab * critique * aleatoire
        print(str(coef_multiplicateur) + "(" + str(efficacite) + ", " + str(stab) + ", " + str(critique) + ", " + str(aleatoire) + ")")
        degats = int((int(int(int(int(pokemon_attaquant.niveau * 0.4) + 2) * att * attaque.puissance / defense) / 50) + 2) * coef_multiplicateur)
        resultat.degat = degats

        return resultat

    @staticmethod
    def calcul_experience(pokemon_attaquant: Pokemon, pokemon_vaincu: Pokemon):
        return int(pokemon_vaincu.base_exp * pokemon_vaincu.niveau / 5)

    @staticmethod
    def calcul_niveau(pokemon: Pokemon):
        def courbe_moyenne(niveau):
            return niveau ** 3

        nouveau_niveau = pokemon.niveau
        if nouveau_niveau < 100:
            while True:
                experience_necessaire = courbe_moyenne(nouveau_niveau + 1)
                if pokemon.experience >= experience_necessaire:
                    nouveau_niveau += 1
                    if nouveau_niveau == 100:
                        break
                else:
                    break

        return nouveau_niveau

    @staticmethod
    def calcul_argent(opposant: Opposant):
        return (1 if opposant.sauvage else 50) * opposant.niveau_max()
