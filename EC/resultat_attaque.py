from classe.attaque import AttaqueCategorie
from classe.type import Affinite


class ResultatAttaque:
    def __init__(self):
        self.attaque_categorie: AttaqueCategorie = AttaqueCategorie.STATUT
        self.degat: int = 0
        self.critique: bool = False
        self.affinite: Affinite = Affinite.NEUTRE
        self.effets = []
