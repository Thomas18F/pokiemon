import pygame


class ControleurAction:
    def __init__(self):
        self.liste_actions = []

    def ajoute_action(self, temps, action):
        self.liste_actions.append((pygame.time.get_ticks() + temps, action))
        if len(self.liste_actions) > 1:
            self.tri_actions()

    def ajoute_action_directe(self, action):
        self.ajoute_action(0, action)

    def tri_actions(self):
        self.liste_actions.sort(key=lambda action: action[0])

    def __get_action_avec_temps(self, temps):
        if len(self.liste_actions) > 0 and self.liste_actions[0][0] < temps:
            action = self.liste_actions.pop(0)
            return action[1]
        else:
            return None

    def get_action(self):
        return self.__get_action_avec_temps(pygame.time.get_ticks())
