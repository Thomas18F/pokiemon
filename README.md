
### Génération de l'exécutable

###### Lancement du venv avec
```bash
venv-pokiemon\Scripts\activate
```

###### Génération de l'exécutable
```bash
pyinstaller --onefile main.py
```
