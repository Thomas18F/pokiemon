

def printo(objet, indentation=0, final=False):
    if type(objet) in [int, float, str, bool]:
        print(" " * indentation + str(objet))
    elif objet is None:
        print(" " * indentation + "None")
    elif type(objet) == list:
        print(" " * indentation + "[")
        for element_liste in objet:
            printo(element_liste, indentation + 2)
        print(" " * indentation + "]")
    elif type(objet) == dict:
        print(" " * indentation + "{")
        for clef in objet:
            print(" " * indentation + str(clef), end=": ")
            printo(objet[clef], indentation + 2, True)
        print(" " * indentation + "}")
    elif isinstance(objet, object):
        try:
            printo(objet.__dict__)
        except:
            print(" " * indentation + "ERREUR")
