import pygame

from parametre import Ecran


class Camera:
    def __init__(self, width, height):
        # Création de notre caméra
        self.camera = pygame.Rect(0, 0, width, height)
        self.width = width
        self.height = height
        print(self.width, self.height, self.width > Ecran.LARGEUR, self.height > Ecran.HAUTEUR)
        self.gel_x = not self.width > Ecran.LARGEUR
        self.gel_y = not self.height > Ecran.HAUTEUR

    def apply(self, entity):
        # Savoir où il faut dessiner l'entity en fct de l'offset * TSIZE
        return entity.rect.move(self.camera.topleft)

    def apply_rect(self, rect):
        # Same
        return rect.move(self.camera.topleft)

    def update(self, target):
        if self.gel_x and self.gel_y:
            self.camera = pygame.Rect(
                (Ecran.LARGEUR - self.width) // 2,
                (Ecran.HAUTEUR - self.height) // 2,
                self.width,
                self.height,
            )
        else:
            # Déplacement de la caméra en fct des offsets (offsets est l'inverse du déplacement du player)
            x = -target.rect.x + int(Ecran.LARGEUR / 2)
            y = -target.rect.y + int(Ecran.HAUTEUR / 2)
            # Définir les limites pour que la caméra s'arrête en bordure de map
            x = min(0, x)
            y = min(0, y)
            x = max(-(self.width - Ecran.LARGEUR), x)
            y = max(-(self.height - Ecran.HAUTEUR), y)
            # Update la caméra en fonction de ce que l'on souhaite à chaque frame
            self.camera = pygame.Rect(x, y, self.width, self.height)
