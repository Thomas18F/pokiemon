import json
import os
from os import path

import pygame
from pygame.sprite import Group
from pygame.surface import Surface

import EC.menu
from EC.texte import Texte
from EC.victoire_type import VictoireType
from cam import Camera
from gestion_musique import GestionMusique
from gestion_touche import GestionTouche
from joueur import Joueur
from parametre import Ecran, Couleur, Params, Touche, Musique
from carte import Tmap


class GestionSauvegarde:
    CHEMIN_SAUVEGARDE = "sauvegarde/joueur.json"

    def __init__(self):
        if not os.path.isfile(GestionSauvegarde.CHEMIN_SAUVEGARDE):
            self.donnees = {}
            self.enregistre()
        with open(GestionSauvegarde.CHEMIN_SAUVEGARDE, "r") as fichier_lecture:
            self.donnees = json.loads(fichier_lecture.read())
            if "joueur" not in self.donnees:
                self.donnees["joueur"] = {}

    def enregistre(self):
        with open("sauvegarde/joueur.json", "w") as fichier_ecriture:
            json.dump(self.donnees, fichier_ecriture, indent=4)


class App:
    def __init__(self):
        # On initialise pygame et les fichiers grâce à load et on crée la fenêtre grâce aux variables WIDTH et HEIGHT
        pygame.init()
        pygame.mixer.init()

        self.screen = pygame.display.set_mode((Ecran.LARGEUR, Ecran.HAUTEUR))
        pygame.display.set_caption("PokIEmon")
        self.clock = pygame.time.Clock()

        # Fonction qui load tous les fichiers (map, images, json)
        dossier_projet = path.dirname(__file__)
        self.mus_folder = path.join(dossier_projet, 'music')

        # On crée un groupe pour les sprites
        self.carte_sprites: Group = Group()
        self.combat: Group = Group()

        self.sauvegarde = GestionSauvegarde()
        self.player = Joueur(
            self,
            self.sauvegarde.donnees["joueur"],
        )

        self.map = None
        self.map_img = None
        self.map_rect = None
        self.camera = None
        self.set_carte(self.player.carte)

        self.EC = EC.menu.EC(self.player)
        self.combat.add(self.EC)
        self.animation_en_cours = False

        print(self.player.carte.width / 64, self.player.carte.height / 64)

        self.gestion_touche = GestionTouche()
        self.gestion_touche.ajout_supervision_touche(Touche.SAC)
        self.gestion_touche.ajout_supervision_touche(Touche.ACTION)
        self.gestion_touche.ajout_supervision_touche(Touche.SON)
        self.gestion_touche.ajout_supervision_touche(Touche.SON_PLUS)
        self.gestion_touche.ajout_supervision_touche(Touche.SON_MOINS)

        GestionMusique.volume(self.player.parametre.musique.volume)

    def set_carte(self, carte: Tmap):
        self.map = carte
        self.map_img = self.map.make_map()
        self.map_rect = self.map_img.get_rect()

        self.camera = Camera(self.map.width, self.map.height)

    def animation_debut_combat(self, surface: Surface, width, height):
        fade_white = pygame.Surface((width, height))
        fade_white.fill((255, 255, 255))
        fade_black = pygame.Surface((width, height))
        fade_black.fill((0, 0, 0))

        pygame.time.delay(50)
        
        for alpha in range(0, 75):
            fade_black.set_alpha(alpha * 2)
            surface.blit(fade_black, (0, 0))
            pygame.display.update()
            pygame.time.delay(4)
            fade_white.set_alpha(alpha * 2)
            surface.blit(fade_white, (0, 0))
            pygame.display.update()
            pygame.time.delay(4)

        self.dessine_jeu()
        pygame.time.delay(50)

    def montre_ecran_accueil(self):
        GestionMusique.sauve()
        GestionMusique.nouveau(Musique.MUSIQUE_MENU)

        self.screen.fill(Couleur.NOIR)
        Texte.affiche_texte(
            self.screen,
            "PokIEmon v" + Ecran.VERSION,
            100,
            Ecran.LARGEUR / 6, (Ecran.HAUTEUR / 2) - 240,
            Couleur.ROUGE,
        )
        Texte.affiche_texte(
            self.screen,
            "Vivez la rencontre étrange entre deux mondes !",
            33,
            (Ecran.LARGEUR / 6) + 10, (Ecran.HAUTEUR / 2) - 190,
            Couleur.ROUGE,
        )
        Texte.affiche_texte(
            self.screen,
            "Appuyez sur une touche pour commencer",
            60,
            70, (Ecran.HAUTEUR / 2) - 60,
            Couleur.VERT,
        )
        Texte.affiche_texte(
            self.screen,
            "Pavé directionnel pour se déplacer",
            24,
            60, (Ecran.HAUTEUR * 4 / 6),
            Couleur.BLANC,
        )
        Texte.affiche_texte(
            self.screen,
            "entrée pour interagir",
            24,
            60, (Ecran.HAUTEUR * 4 / 6) + 24,
            Couleur.BLANC,
        )
        Texte.affiche_texte(
            self.screen,
            "a, z, e pour le son",
            24,
            60, (Ecran.HAUTEUR * 4 / 6) + 48,
            Couleur.BLANC,
        )
        Texte.affiche_texte(
            self.screen,
            "Création du jeu : thomas18F",
            24,
            30, (Ecran.HAUTEUR * 5 / 6) + 70,
            Couleur.BLANC,
        )
        pygame.display.flip()
        self.wait_for_key()

        GestionMusique.arret()
        GestionMusique.restaure()

    def wait_for_key(self):
        pygame.event.wait()
        waiting = True
        while waiting:
            self.clock.tick(Ecran.FPS)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                if event.type == pygame.KEYUP:
                    waiting = False

    def lancement(self):
        jeu_en_cours = True
        self.montre_ecran_accueil()
        while jeu_en_cours:
            # On fixe les FPS du jeu
            self.time = self.clock.tick(Ecran.FPS) / 1000

            # Récupération des inputs
            for event in pygame.event.get():
                # Pour fermer la fenêtre, on arrête la boucle while
                if event.type == pygame.QUIT:
                    jeu_en_cours = False

            self.gestion_touche.maj()

            if self.EC.en_combat:
                if not self.animation_en_cours:
                    if Params.MUSIQUE and self.player.parametre.musique.allume:
                        GestionMusique.sauve()
                        GestionMusique.nouveau(self.EC.opposant.musique + ".mp3")
                    if Params.ANIMATION_COMBAT:
                        self.animation_debut_combat(self.screen, 1500, 1500)
                    self.animation_en_cours = True
                self.combat.draw(self.screen)
                self.EC.maj()

                if self.EC.vainqueur != VictoireType.NON_TERMINE:
                    self.animation_en_cours = False
                    GestionMusique.transition_restauration(Musique.TEMPS_DISPARITION)
                    self.EC.en_combat = False

            elif self.player.dialogue:
                self.player.doit_terminer_dialogue()

            else:
                self.dessine_jeu()

            if self.gestion_touche.est_pressee(Touche.SON):
                self.player.parametre.active_desactive_son()
            if self.gestion_touche.est_pressee(Touche.SON_MOINS):
                self.player.parametre.diminue_son()
            if self.gestion_touche.est_pressee(Touche.SON_PLUS):
                self.player.parametre.augmente_son()

            # Une fois que tout est dessiné, on l'affiche à l'écran
            pygame.display.flip()

        self.sauvegarde.donnees["version"] = Ecran.VERSION
        self.sauvegarde.donnees["joueur"] = self.player.serialise()

        self.sauvegarde.enregistre()

        pygame.quit()

    def dessine_jeu(self):
        # Update de la caméra en fonction du joueur puisqu'elle le suit
        self.camera.update(self.player)

        # La map est dessinée
        self.screen.fill(Couleur.NOIR)
        self.screen.blit(self.map_img, self.camera.apply_rect(self.map_rect))

        self.dessine_sur_carte()

    def dessine_sur_carte(self):
        meta_sprites = [[image.sprite, image.niveau, image.x] for image in self.map.surimpression]
        meta_sprites += [[self.player, 5, self.player.coordonnes.x]]

        meta_sprites.sort(key=lambda sprite_meta: (sprite_meta[1], -sprite_meta[2]))

        for meta_sprite in meta_sprites:
            self.screen.blit(meta_sprite[0].image, self.camera.apply(meta_sprite[0]))

        self.carte_sprites.update()
