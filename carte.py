import json
from os import path
from typing import Dict, Union

import pygame
import pytmx
from pygame import image
from pygame.sprite import Sprite, Group
from pygame.surface import Surface

from gestion_musique import GestionMusique
from maths import Coordonnee
from parametre import Ecran, Couleur


class Tmap:
    def __init__(self, nom, donnees, app, joueur):
        # Initialisation de la map et load
        self.carte_sprites: Group = app.carte_sprites
        tm = pytmx.load_pygame(path.join("map", donnees["carte"]), pixelalpha=True)
        self.nom = nom
        self.version = donnees["version"]
        self.nom_musique = donnees["musique"]
        GestionMusique.nouveau(self.nom_musique + ".mp3")
        self.width = tm.width * tm.tilewidth
        self.height = tm.height * tm.tileheight
        self.tmxdata = tm
        self.__hitbox = donnees["hitbox"]
        self.zone = donnees.get("zone", [])
        self.dresseur = donnees.get("dresseur", [])
        self.surimpression = [SurimpressionImage(donnee) for donnee in donnees.get("surimpression", [])]
        self.surpasse_hitbox()
        self.traitement_special(joueur)

    def traitement_special(self, joueur):
        for (x, colonne) in enumerate(self.__hitbox):
            for (y, case) in enumerate(colonne):
                if case["type"] == "rocher_trou":
                    if joueur.valeurs.est_vrai(self.nom, case["valeur"]):
                        dico = {
                            "x": x,
                            "y": y,
                            "niveau": 3,
                            "image": case["actif"],
                            "special_collision": {"type": "libre"}
                        }

                        self.ajoute_surimpression_image(dico)

        for surimpression in self.surimpression:
            if surimpression.desactivation_valeur is not None:
                if joueur.valeurs.est_vrai(self.nom, surimpression.desactivation_valeur):
                    self.surimpression.remove(surimpression)

    def surpasse_hitbox(self):
        for surimpression in self.surimpression:
            if surimpression.special_collision is not None:
                self.__hitbox[surimpression.x][surimpression.y] = surimpression.special_collision

    def get_surimpression_coo_nom(self, x: int, y: int, nom: str):
        for surimpression in self.surimpression:
            if surimpression.x == x and surimpression.y == y and surimpression.nom_image == nom:
                return surimpression
        return None

    @staticmethod
    def avec_nom(nom_carte, app, joueur):
        with open("map/"+nom_carte+".json", "r") as fichier:
            carte_donnees_json = json.load(fichier)
            tmap: Tmap = Tmap(nom_carte, carte_donnees_json, app, joueur)
            return tmap

    def ajoute_surimpression_image(self, surimpression_dict: Dict):
        surimpression_image = SurimpressionImage(surimpression_dict)
        if surimpression_image.special_collision is not None:
            self.__hitbox[surimpression_image.x][surimpression_image.y] = surimpression_image.special_collision
        self.surimpression.append(surimpression_image)

    def hitbox(self, x: Union[int, Coordonnee], y: Union[int, None] = None):
        if type(x) == Coordonnee:
            return self.__hitbox[x.x][x.y]
        if type(x) == int:
            return self.__hitbox[x][y]

    def set_hitbox(self, valeur, x, y):
        self.__hitbox[x][y] = valeur

    def get_zone(self, id_zone):
        for zone in self.zone:
            if zone["id"] == id_zone:
                return zone

    def get_dresseur(self, id_dresseur):
        for dresseur in self.dresseur:
            if dresseur["id"] == id_dresseur:
                return dresseur

    def render(self, surface):
        # Contre rendu de toutes les positions des images sur la map + affichage par blit
        ti = self.tmxdata.get_tile_image_by_gid
        for layer in self.tmxdata.visible_layers:
            if isinstance(layer, pytmx.TiledTileLayer):
                for x, y, gid, in layer:
                    tile = ti(gid)
                    if tile:
                        surface.blit(tile, (x * self.tmxdata.tilewidth, y * self.tmxdata.tileheight))

    def make_map(self):
        # Retourner la map pour l'afficher
        temp_surface = pygame.Surface((self.width, self.height))
        self.render(temp_surface)
        return temp_surface


class SurimpressionImage:
    def __init__(self, donnees_surimpression: Dict):
        print(donnees_surimpression)
        self.x: int = donnees_surimpression["x"]
        self.y: int = donnees_surimpression["y"]
        self.niveau: Union[int, float] = donnees_surimpression["niveau"]
        self.nom_image: str = donnees_surimpression["image"]
        self.special_collision: Union[Dict, None] = donnees_surimpression.get("special_collision", None)
        self.desactivation_valeur: Union[str, None] = donnees_surimpression.get("desactivation_valeur", None)
        self.sprite: Sprite = self.initialise_sprite()

    def initialise_sprite(self):
        sprite = Sprite()
        surface = Surface((Ecran.TAILLE_TUILE, Ecran.TAILLE_TUILE))
        surimpression_image = image.load(
            path.join("map", "ressources", "sii", self.nom_image + ".png")
        ).convert_alpha()
        rectangle = surface.get_rect()
        rectangle.x = self.x * Ecran.TAILLE_TUILE
        rectangle.y = self.y * Ecran.TAILLE_TUILE
        sprite.image = surimpression_image
        sprite.image.set_colorkey(Couleur.ROSE)
        surface.blit(surimpression_image, rectangle)
        sprite.rect = rectangle

        return sprite

    def deplace(self, coos: Coordonnee):
        self.x = coos.x
        self.y = coos.y
        self.sprite.rect.x = self.x * Ecran.TAILLE_TUILE
        self.sprite.rect.y = self.y * Ecran.TAILLE_TUILE
