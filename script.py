import json

import requests

from outil import printo


def __get_donnees_capacite(id_capacite: int):
    return requests.get("https://pokeapi.co/api/v2/move/" + str(id_capacite)).json()


def __get_donnees_pokemon(id_pokemon: int):
    return requests.get("https://pokeapi.co/api/v2/pokemon/" + str(id_pokemon)).json()


def affiche_donnees_pokemon(id_pokemon: int):
    printo(__get_donnees_pokemon(id_pokemon))


#affiche_donnees_pokemon(1)


def type_traduction(type_pokemon: str):
    conversion = {
        "normal": "Normal",
        "grass": "Plante",
        "fire": "Feu",
        "water": "Eau",
        "fighting": "Combat",
        "poison": "Poison",
        "flying": "Vol",
        "electric": "Electrique",
        "ground": "Sol",
        "rock": "Roche",
        "psychic": "Psy",
        "ice": "Glace",
        "bug": "Insecte",
        "dragon": "Dragon",
        "ghost": "Spectre",
        "dark": "Tenebre",
        "steel": "Acier",
        "fairy": "Fee",
    }

    return conversion.get(type_pokemon, "")


def categorie_traduction(categorie: str):
    conversion = {
        "physical": "physique",
        "special": "special",
        "status": "statut",
    }

    return conversion.get(categorie, categorie)


def analyse_donnees_pokemon(id_pokemon: int):
    donnees = __get_donnees_pokemon(id_pokemon)

    analyse = {
        "id": donnees["id"],
        "nom": donnees["forms"][0]["name"],
        "types": [type_traduction(type_pok["type"]["name"]) for type_pok in donnees["types"]]
    }

    for stat in donnees["stats"]:
        if stat["stat"]["name"] == "hp":
            analyse["stat_pv"] = stat["base_stat"]
        elif stat["stat"]["name"] == "attack":
            analyse["stat_att"] = stat["base_stat"]
        elif stat["stat"]["name"] == "defense":
            analyse["stat_defense"] = stat["base_stat"]
        elif stat["stat"]["name"] == "special-attack":
            analyse["stat_att_spe"] = stat["base_stat"]
        elif stat["stat"]["name"] == "special-defense":
            analyse["stat_def_spe"] = stat["base_stat"]
        elif stat["stat"]["name"] == "speed":
            analyse["stat_vit"] = stat["base_stat"]
    analyse["capacites"] = [capacite["move"]["url"].split("/")[-2] for capacite in donnees["moves"]]
    analyse["base_exp"] = donnees["base_experience"]

    return analyse


def analyse_donnees_capacite(id_capacite: int):
    donnees = __get_donnees_capacite(id_capacite)

    analyse = {
        "id": donnees["id"],
    }
    for nom in donnees["names"]:
        if nom["language"]["name"] == "fr":
            analyse["nom"] = nom["name"]
            break
    analyse["type_att"] = type_traduction(donnees["type"]["name"])
    analyse["puissance"] = donnees["power"]
    analyse["pp"] = donnees["pp"]
    analyse["categorie"] = categorie_traduction(donnees["damage_class"]["name"])

    return analyse


#printo(analyse_donnees_pokemon(1))


def ajout_pokemon(id_pokemon: int):
    with open("donnees/pokemon.json", "r") as fichier_lecture:
        donnees_existante = json.loads(fichier_lecture.read())

    for pokemon in donnees_existante:
        if pokemon["id"] == id_pokemon:
            print("le pokemon existe déjà")
            return False

    donnees_existante.append(analyse_donnees_pokemon(id_pokemon))

    donnees_existante.sort(key=lambda donnees_pokemon: donnees_pokemon["id"])

    with open("donnees/pokemon.json", "w") as fichier_ecriture:
        json.dump(donnees_existante, fichier_ecriture, indent=4)

    print("pokemon ajouté")
    return True


def ajout_capacite(id_capacite: int):
    with open("donnees/attaque.json", "r") as fichier_lecture:
        donnees_existante = json.loads(fichier_lecture.read())

    for attaque in donnees_existante:
        if attaque["id"] == id_capacite:
            print("l'attaque existe déjà")
            return False

    donnees_existante.append(analyse_donnees_capacite(id_capacite))

    donnees_existante.sort(key=lambda donnees_attaque: donnees_attaque["id"])

    with open("donnees/attaque.json", "w") as fichier_ecriture:
        json.dump(donnees_existante, fichier_ecriture, indent=4)

    print("attaque ajoutée")
    return True


#ajout_pokemon(296)
#printo(analyse_donnees_capacite(50))
ajout_capacite(73)
