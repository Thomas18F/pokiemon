
import pygame
import random

from pygame.sprite import Sprite

from EC.opposant import Opposant
from EC.texte import Texte
from classe.collection_sprites import CollectionSprites
from classe.dialogue import Dialogue
from classe.gestion_valeur import GestionValeur
from classe.objet import CT, Objet
from classe.parametre_joueur import ParametreJoueur
from classe.pokemon import Pokemon
from classe.sac import Sac
from maths import Coordonnee
from orientation import Orientation
from parametre import Couleur, Ecran, Params, Touche
from carte import Tmap, SurimpressionImage


class Joueur(Sprite):
    def __init__(self, app, donnees_sauvegardees):
        # Directement add au group
        Sprite.__init__(self, app.carte_sprites)
        self.app = app
        # création du joueur et sa surface
        self.image = pygame.Surface((Ecran.TAILLE_TUILE, Ecran.TAILLE_TUILE))
        self.rect = self.image.get_rect()
        self.rect.y += Ecran.DECALAGE_SPRITE_JOUEUR_HAUT

        self.buffer = None

        self.valeurs: GestionValeur = GestionValeur(donnees_sauvegardees.get("valeurs", {}))

        self.coordonnes: Coordonnee = Coordonnee(donnees_sauvegardees.get("x", 29), donnees_sauvegardees.get("y", 31))
        self.coordonnes_sauvegarde: Coordonnee = Coordonnee(donnees_sauvegardees.get("x_sauve", 29), donnees_sauvegardees.get("y_sauve", 31))
        self.carte: Tmap = Tmap.avec_nom(donnees_sauvegardees.get("carte", "route_1"), app, self)
        self.carte_sauvegarde_nom = donnees_sauvegardees.get("carte_sauvegarde", "route_1")

        self.argent: int = donnees_sauvegardees.get("argent", 0)
        self.sac: Sac = Sac(donnees_sauvegardees.get("sac", {}))
        self.parametre: ParametreJoueur = ParametreJoueur(donnees_sauvegardees.get("parametre", None))

        self.frames = CollectionSprites("RED_rose")
        self.orientation: int = Orientation.BAS
        self.pokemons = [Pokemon.from_mix(donnees_pokemon) for donnees_pokemon in donnees_sauvegardees.get("pokemons", [])]

        self.case_combat = 0
        self.dialogue = False

        self.actualise_position_rectangle()

    def serialise(self):
        return {
            "carte": self.carte.nom,
            "x": self.coordonnes.x,
            "y": self.coordonnes.y,
            "carte_sauve": self.carte_sauvegarde_nom,
            "x_sauve": self.coordonnes_sauvegarde.x,
            "y_sauve": self.coordonnes_sauvegarde.y,
            "argent": self.argent,
            "pokemons": [pokemon.serialise() for pokemon in self.pokemons],
            "sac": self.sac.serialise(),
            "valeurs": self.valeurs.serialise(),
            "parametre": self.parametre.serialise(),
        }

    def actualise_position_rectangle(self):
        # Actualisation des positions pour renvoyer les infos à la fct des collisions
        self.rect.x = self.coordonnes.x * Ecran.TAILLE_TUILE
        self.rect.y = self.coordonnes.y * Ecran.TAILLE_TUILE + Ecran.DECALAGE_SPRITE_JOUEUR_HAUT

    def determine_combat(self, id_zone: int):
        if Params.RENCONTRE_ALEATOIRE and self.case_combat > 3:
            zone = self.carte.get_zone(id_zone)
            pourcentage_declenchement_combat = zone["proba"]

            en_combat = True if random.randint(0, 100) <= pourcentage_declenchement_combat else False
            if en_combat:
                self.case_combat = 0
                opposant: Opposant = Opposant.pokemon_sauvage(Pokemon.from_id(2, 5))

                self.app.EC.lance_combat(opposant)

    def update(self):
        Sprite.update(self)

        # On récupère toutes les touches pressées à cette frame
        keys_pressed = pygame.key.get_pressed()
        self.image = self.frames.get()
        self.image.set_colorkey(Couleur.ROSE)

        if self.app.gestion_touche.est_pressee(Touche.SAC):
            print("sac")
            if len(self.sac.CTs) != 0:
                ct: CT = self.sac.CTs[0]
                self.pokemons[0].capacites.append(ct.attaque)

        if self.app.gestion_touche.est_pressee(Touche.ACTION):
            print("action")

            case_vu: Coordonnee = Coordonnee(self.coordonnes.x, self.coordonnes.y)

            if self.orientation == Orientation.GAUCHE:
                case_vu.x -= 1
            elif self.orientation == Orientation.DROITE:
                case_vu.x += 1
            elif self.orientation == Orientation.HAUT:
                case_vu.y -= 1
            elif self.orientation == Orientation.BAS:
                case_vu.y += 1

            donnees_vu = self.carte.hitbox(case_vu)
            if donnees_vu["type"] == "panneau":
                texte = Dialogue.dialogue_avec_id(donnees_vu["id_texte"])

                self.dialogue = True
                Texte.affiche_dialogue(self.app.screen, texte)

            elif donnees_vu["type"] == "objet":
                if not self.dialogue:
                    if not self.valeurs.est_vrai(self.carte.nom, donnees_vu["valeur"]):
                        self.valeurs.ajoute_valeur(self.carte.nom, donnees_vu["valeur"])

                        objet = Objet.from_id(donnees_vu["id_objet"])
                        self.sac.ajout_objet(objet)
                        if objet is not None:
                            self.dialogue = True
                            Texte.affiche_dialogue(self.app.screen, "Vous avez trouvé " + objet.texte)

            elif donnees_vu["type"] == "dresseur":
                if self.buffer is None:
                    self.lance_combat_dresseur(donnees_vu)

        deplacement: Coordonnee = Coordonnee(0, 0)
        if keys_pressed[Touche.DEPLACEMENT_GAUCHE]:
            if self.orientation != Orientation.GAUCHE:
                self.frames.nouveau("gauche")
                self.orientation = Orientation.GAUCHE
            else:
                self.frames.suivant()
                deplacement.x -= 1

        elif keys_pressed[Touche.DEPLACEMENT_DROITE]:
            if self.orientation != Orientation.DROITE:
                self.frames.nouveau("droite")
                self.orientation = Orientation.DROITE
            else:
                self.frames.suivant()
                deplacement.x += 1

        elif keys_pressed[Touche.DEPLACEMENT_HAUT]:
            if self.orientation != Orientation.HAUT:
                self.frames.nouveau("haut")
                self.orientation = Orientation.HAUT
            else:
                self.frames.suivant()
                deplacement.y -= 1

        elif keys_pressed[Touche.DEPLACEMENT_BAS]:
            if self.orientation != Orientation.BAS:
                self.frames.nouveau("bas")
                self.orientation = Orientation.BAS
            else:
                self.frames.suivant()
                deplacement.y += 1

        else:
            return

        self.image = self.frames.get()
        self.image.set_colorkey(Couleur.ROSE)
        pygame.time.delay(Ecran.TEMPS_DEPLACEMENT)

        self.case_combat += 1

        coordonnes_temporaire: Coordonnee = self.coordonnes + deplacement
        print(self.coordonnes, deplacement, coordonnes_temporaire)

        if Params.SANS_COLLISION:
            self.coordonnes = coordonnes_temporaire
            self.actualise_position_rectangle()
            return

        position_est_actualisee: bool = False

        donnees_case_depart = self.carte.hitbox(self.coordonnes)

        if donnees_case_depart["type"] == "libre_mais":
            if self.orientation == Orientation.HAUT and "haut" in donnees_case_depart and not donnees_case_depart["haut"]:
                return
            if self.orientation == Orientation.BAS:
                if "bas" in donnees_case_depart and not donnees_case_depart["bas"]:
                    return
                if "teleportation" in donnees_case_depart:
                    self.teleportation_vers_avec_donnees(donnees_case_depart["teleportation"])
                    return
            if self.orientation == Orientation.GAUCHE and "gauche" in donnees_case_depart and not donnees_case_depart["gauche"]:
                return
            if self.orientation == Orientation.DROITE and "droite" in donnees_case_depart and not donnees_case_depart["droite"]:
                return

        donnees_case_destination = self.carte.hitbox(coordonnes_temporaire)
        print(donnees_case_destination)

        if donnees_case_destination["type"] == "mur":
            pass
        elif donnees_case_destination["type"] == "libre" or donnees_case_destination["type"] == "libre_mais":
            self.coordonnes = coordonnes_temporaire
        elif donnees_case_destination["type"] == "herbe":
            self.coordonnes = coordonnes_temporaire

            position_est_actualisee = True
            self.actualise_position_rectangle()

            self.determine_combat(donnees_case_destination["id_zone"])
        elif donnees_case_destination["type"] == "teleportation":
            self.teleportation_vers_avec_donnees(donnees_case_destination)
            position_est_actualisee = True

        elif donnees_case_destination["type"] == "bord":
            self.coordonnes = coordonnes_temporaire
            self.actualise_position_rectangle()

            pygame.time.delay(Ecran.TEMPS_DEPLACEMENT)

            if donnees_case_destination["direction"] == "bas":
                self.coordonnes.y += 1
            elif donnees_case_destination["direction"] == "droite":
                self.coordonnes.x += 1
            elif donnees_case_destination["direction"] == "haut":
                self.coordonnes.y -= 1
            elif donnees_case_destination["direction"] == "gauche":
                self.coordonnes.x -= 1

        elif donnees_case_destination["type"] == "eau":
            if donnees_case_depart["type"] == "eau":
                self.coordonnes = coordonnes_temporaire
            else:
                for pokemon in self.pokemons:
                    for capacite in pokemon.capacites:
                        if capacite.nom == "Surf":
                            self.coordonnes = coordonnes_temporaire

        elif donnees_case_destination["type"] == "rocher_force":
            for pokemon in self.pokemons:
                for capacite in pokemon.capacites:
                    if capacite.nom == "Force":
                        destination: Coordonnee = Coordonnee(
                            2 * coordonnes_temporaire.x - self.coordonnes.x,
                            2 * coordonnes_temporaire.y - self.coordonnes.y
                        )
                        donnees_destination = self.carte.hitbox(destination)
                        if donnees_destination["type"] == "libre" or donnees_destination["type"] == "rocher_trou":
                            self.carte.set_hitbox(
                                {"type": "libre"},
                                coordonnes_temporaire.x,
                                coordonnes_temporaire.y,
                            )

                            self.carte.set_hitbox(
                                {"type": "rocher_force"},
                                destination.x,
                                destination.y,
                            )
                            rocher: SurimpressionImage = self.carte.get_surimpression_coo_nom(
                                coordonnes_temporaire.x,
                                coordonnes_temporaire.y,
                                "rocher_force",
                            )
                            rocher.deplace(destination)

                            if donnees_destination["type"] == "rocher_trou":
                                print(donnees_destination)
                                dico = {
                                    "x": destination.x,
                                    "y": destination.y,
                                    "niveau": 3,
                                    "image": donnees_destination["actif"],
                                    "special_collision": {"type": "libre"}
                                }

                                self.carte.ajoute_surimpression_image(dico)
                                self.valeurs.ajoute_valeur(self.carte.nom, donnees_destination["valeur"])

                            self.coordonnes = coordonnes_temporaire

        elif donnees_case_destination["type"] == "rocher_eclate":
            for pokemon in self.pokemons:
                for capacite in pokemon.capacites:
                    if capacite.nom == "Éclate-Roc":
                        self.carte.set_hitbox(
                            {"type": "libre"},
                            coordonnes_temporaire.x,
                            coordonnes_temporaire.y,
                        )

                        rocher: SurimpressionImage = self.carte.get_surimpression_coo_nom(
                            coordonnes_temporaire.x,
                            coordonnes_temporaire.y,
                            "rocher_eclate",
                        )

                        self.carte.surimpression.remove(rocher)

        elif donnees_case_destination["type"] == "dresseur_vision":
            self.coordonnes = coordonnes_temporaire

            position_est_actualisee = True
            self.actualise_position_rectangle()

            self.lance_combat_dresseur(donnees_case_destination)

        if not position_est_actualisee:
            self.actualise_position_rectangle()

    def lance_combat_dresseur(self, donnees_dresseur):
        opposant: Opposant = Opposant.dresseur(self.carte.get_dresseur(donnees_dresseur["id_dresseur"]))

        if self.valeurs.est_vrai(self.carte.nom, opposant.valeur):
            return

        self.dialogue = True
        Texte.affiche_dialogue(self.app.screen, opposant.pre_combat)

        self.buffer = opposant

    def doit_terminer_dialogue(self):
        if self.app.gestion_touche.est_pressee(Touche.ACTION):

            if self.dialogue:
                self.dialogue = False

                if self.buffer is not None:
                    self.app.dessine_jeu()

                    self.app.EC.lance_combat(self.buffer)
                    self.buffer = None

    def soin_total(self):
        for pokemon in self.pokemons:
            pokemon.PV_actuel = pokemon.PV

    def teleportation_vers_avec_donnees(self, donnees):
        print(donnees["carte"], donnees["vers_x"], donnees["vers_y"])
        self.teleportation_vers(donnees["carte"], donnees["vers_x"], donnees["vers_y"])

    def teleportation_vers(self, carte, x, y):
        if carte is not None:
            nouvelle_carte = Tmap.avec_nom(carte, self.app, self)
            self.carte: Tmap = nouvelle_carte
            self.app.set_carte(self.carte)
        self.coordonnes = Coordonnee(x, y)

        self.actualise_position_rectangle()
