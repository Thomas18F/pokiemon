import time
from os import path
from typing import Final, Union

import pygame


class GestionMusique:
    DOSSIER_MUSIQUE: Final[str] = "music"

    __actuel: Union[str, None] = None
    __volume: float = 0.5
    __sauvegarde: Union[str, None] = None

    @staticmethod
    def nouveau(nom: str):
        if nom != GestionMusique.__actuel:
            GestionMusique.__actuel = nom
            pygame.mixer_music.load(path.join(GestionMusique.DOSSIER_MUSIQUE, nom))
            pygame.mixer_music.set_volume(GestionMusique.__volume)
            pygame.mixer_music.play(-1)

    @staticmethod
    def volume(valeur: float):
        GestionMusique.__volume = valeur
        pygame.mixer_music.set_volume(GestionMusique.__volume)

    @staticmethod
    def arret():
        GestionMusique.__actuel = None
        pygame.mixer_music.stop()

    @staticmethod
    def arret_fondu(temps: int):
        pygame.mixer_music.fadeout(temps)
        time.sleep(temps/1000)
        GestionMusique.arret()

    @staticmethod
    def sauve():
        GestionMusique.__sauvegarde = GestionMusique.__actuel

    @staticmethod
    def restaure():
        GestionMusique.nouveau(GestionMusique.__sauvegarde)
        GestionMusique.__sauvegarde = None

    @staticmethod
    def transition(nom: str, temps: int):
        GestionMusique.arret_fondu(temps)
        GestionMusique.nouveau(nom)

    @staticmethod
    def transition_restauration(temps: int):
        GestionMusique.transition(GestionMusique.__sauvegarde, temps)
        GestionMusique.__sauvegarde = None
