
class Coordonnee:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y

    def __add__(self, other):
        x = self.x + other.x
        y = self.y + other.y
        return Coordonnee(x, y)

    def __iadd__(self, other):
        self.x = other.x
        self.y = other.y
        return self

    def __str__(self):
        return f"({self.x}, {self.y})"
