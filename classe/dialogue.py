import json


class DialogueDonnees:
    def __init__(self):
        with open("donnees/dialogue.json", "r") as fichier_lecture:
            self.donnees = json.loads(fichier_lecture.read())

    def get_donneees_avec_id(self, id_dialogue):
        for donnee in self.donnees:
            if donnee["id"] == id_dialogue:
                return donnee


class Dialogue:
    DONNEES = DialogueDonnees()

    @staticmethod
    def dialogue_avec_id(id_dialogue):
        return Dialogue.DONNEES.get_donneees_avec_id(id_dialogue)["texte"]
