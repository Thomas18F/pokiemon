from typing import Union, Dict

from gestion_musique import GestionMusique
from parametre import Musique


class ParametreJoueur:
    def __init__(self, donnees: Union[Dict, None]):
        if donnees is None:
            self.musique = ParametreMusique.sans_donnees()
        else:
            self.musique = ParametreMusique.avec_donnees(donnees["musique"])

    def serialise(self):
        return {
            "musique": self.musique.serialise()
        }

    def augmente_son(self):
        self.musique.augmente_son()

    def diminue_son(self):
        self.musique.diminue_son()

    def active_desactive_son(self):
        self.musique.active_desactive_son()


class ParametreMusique:
    def __init__(self, allume: bool, volume: float):
        self.allume: bool = allume
        self.volume: float = volume

    @staticmethod
    def sans_donnees():
        return ParametreMusique(
            True,
            Musique.VOLUME_DEFAUT,
        )

    @staticmethod
    def avec_donnees(donnees: Dict):
        return ParametreMusique(
            donnees["allume"],
            donnees["volume"],
        )

    def serialise(self):
        return self.__dict__

    def augmente_son(self):
        self.volume = min(1.0, self.volume + 0.1)
        GestionMusique.volume(self.volume)

    def diminue_son(self):
        self.volume = max(0.0, self.volume - 0.1)
        GestionMusique.volume(self.volume)

    def active_desactive_son(self):
        self.allume = not self.allume
