from typing import Union

from classe.objet import CT, Ball, Objet


class Sac:
    def __init__(self, donnees_sauvegarde):
        self.CTs = []
        self.balls = []

        for categorie in donnees_sauvegarde:
            for element in donnees_sauvegarde[categorie]:
                objet = Objet.from_id(element["identifiant"])
                self.ajout_objet(objet)

                if categorie == "balls":
                    objet.nombre = element["nombre"]

    def serialise(self):
        dictionnaire = self.__dict__

        for categorie in dictionnaire:
            dictionnaire[categorie] = [objet.serialise() for objet in dictionnaire[categorie]]

        return dictionnaire

    def ajout_objet(self, objet: Union[CT, Ball]):
        if type(objet) == CT:
            a_ajouter: bool = True
            for ct in self.CTs:
                if ct.id_attaque > objet.id_attaque:
                    break
                if ct.id_attaque == objet.id_attaque:
                    a_ajouter = False
                    break

            if a_ajouter:
                self.CTs.append(objet)
                self.__tri_ct()

        elif type(objet) == Ball:
            a_ajouter: bool = True
            for ball in self.balls:
                if ball.identifiant > objet.identifiant:
                    break
                if ball.identifiant == objet.identifiant:
                    a_ajouter = False
                    ball.nombre += objet.nombre
                    break

            if a_ajouter:
                self.balls.append(objet)
                self.__tri_ball()

    def __tri_ct(self):
        self.CTs.sort(key=lambda ct: ct.id_attaque)

    def __tri_ball(self):
        self.balls.sort(key=lambda ball: ball.identifiant)
