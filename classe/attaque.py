import json
from typing import List

from classe.type import Type, Types


class AttaqueCategorie:
    PHYSIQUE = 0
    SPECIAL = 1
    STATUT = 2


class AttaqueDonnees:
    def __init__(self):
        with open("donnees/attaque.json", "r") as fichier_lecture:
            self.donnees = json.loads(fichier_lecture.read())

    def get_donneees_avec_id(self, id_attaque):
        for donnee in self.donnees:
            if donnee["id"] == id_attaque:
                return donnee


class Attaque:
    DONNEES = AttaqueDonnees()

    def __init__(self, id_att: int, nom: str, type_att: Type, puissance: int, pp: int, categorie: int, effets: List, **params):
        self.id = id_att
        self.nom = nom
        self.type_att = type_att
        self.puissance = puissance
        self.pp = pp
        self.pp_actuel = params.get("pp_actuel", pp)
        self.categorie = categorie
        self.effets = effets

    def serialise(self):
        return {"id": self.id, "pp_actuel": self.pp_actuel}

    @staticmethod
    def from_id(id_attaque: int):
        donnees = Attaque.DONNEES.get_donneees_avec_id(id_attaque)
        return Attaque(
            id_attaque,
            donnees["nom"],
            Types.get_type(donnees["type_att"]),
            donnees["puissance"],
            donnees["pp"],
            AttaqueCategorie.PHYSIQUE if donnees["categorie"] == "physique" else AttaqueCategorie.STATUT if donnees["categorie"] == "statut" else AttaqueCategorie.SPECIAL,
            [] if "effets" not in donnees else donnees["effets"],
        )
