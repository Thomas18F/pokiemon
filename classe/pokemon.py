import json
from os import path
from typing import Union, List

from pygame import transform, image
from pygame.sprite import Sprite
from pygame.surface import Surface

from EC.parametre_combat import EcranCombat
from classe.attaque import Attaque
from classe.type import Type, Types
from parametre import Couleur


class PokemonDonnees:
    def __init__(self):
        with open("donnees/pokemon.json", "r") as fichier_lecture:
            self.donnees = json.loads(fichier_lecture.read())

    def get_donneees_avec_id(self, id_pokemon: Union[int, float]):
        for donnee in self.donnees:
            if donnee["id"] == id_pokemon:
                return donnee


class PokemonCalculateur:
    @staticmethod
    def calcul_pv(stat_pv, niveau_pokemon):
        return int(((2 * stat_pv * niveau_pokemon) / 100) + niveau_pokemon + 10)

    @staticmethod
    def calcul_stat(stat, niveau_pokemon):
        return int((2 * stat * niveau_pokemon) / 100) + 5


class Pokemon:
    DOSSIER_IMAGE = path.join("EC", "img", "pokemon")
    DONNEES = PokemonDonnees()

    def __init__(self, id_pokemon, nom, niveau, experience, types: List[Type], stat_pv, stat_att, stat_defense,
                 stat_att_spe, stat_def_spe, stat_vit, capacites: List[Attaque], base_exp, **params):
        self.id = id_pokemon
        self.nom = nom
        self.fichier: str = str(id_pokemon) + ".png"
        surface, sprite = self.creation_image()
        self.image = sprite
        self.surface = surface
        self.surface_inversion: bool = False
        self.niveau = niveau
        self.experience = experience
        self.types = types
        self.stat_pv = stat_pv
        self.PV = PokemonCalculateur.calcul_pv(self.stat_pv, niveau)
        self.PV_actuel = params.get("PV_actuel", self.PV)
        if self.PV_actuel is None:
            self.PV_actuel = self.PV
        self.stat_att = stat_att
        self.att = PokemonCalculateur.calcul_stat(self.stat_att, niveau)
        self.stat_defense = stat_defense
        self.defense = PokemonCalculateur.calcul_stat(self.stat_defense, niveau)
        self.stat_att_spe = stat_att_spe
        self.att_spe = PokemonCalculateur.calcul_stat(self.stat_att_spe, niveau)
        self.stat_def_spe = stat_def_spe
        self.def_spe = PokemonCalculateur.calcul_stat(self.stat_def_spe, niveau)
        self.stat_vit = stat_vit
        self.vit = PokemonCalculateur.calcul_stat(self.stat_vit, niveau)
        self.capacites = capacites
        self.base_exp = base_exp
        self.statuts = []

    def creation_image(self):
        sprite = Sprite()
        surface = Surface((EcranCombat.TAILLE_POKEMON_X, EcranCombat.TAILLE_POKEMON_Y))
        pok_img = image.load(path.join(Pokemon.DOSSIER_IMAGE, self.fichier)).convert_alpha()
        pok_img = transform.scale(pok_img, (EcranCombat.TAILLE_POKEMON_X, EcranCombat.TAILLE_POKEMON_Y))
        rect = surface.get_rect()
        surface.blit(pok_img, rect)
        surface.set_colorkey(Couleur.NOIR)
        sprite.image = pok_img
        sprite.rect = rect

        return surface, sprite

    def place(self, x: int, y: int, **params):
        rectangle = self.image.rect
        rectangle.centerx = x
        rectangle.centery = y
        if params.get("tourne", False) and not self.surface_inversion:
            self.surface = transform.flip(self.surface, True, False)
            self.surface_inversion = not self.surface_inversion

    def serialise(self):
        dictionnaire = self.__dict__
        for champ in [
            "fichier", "image", "surface", "surface_inversion", "PV", "att", "defense", "att_spe", "def_spe", "vit",
            "stat_pv", "stat_att", "stat_defense", "stat_att_spe", "stat_def_spe", "stat_vit"
        ]:
            dictionnaire.pop(champ)
        dictionnaire["capacites"] = [capacite.serialise() for capacite in dictionnaire["capacites"]]
        dictionnaire["types"] = [type_pokemon.serialise() for type_pokemon in dictionnaire["types"]]

        return dictionnaire

    @staticmethod
    def from_id(id_pokemon: Union[int, float], niveau: int = 1):
        donnees = Pokemon.DONNEES.get_donneees_avec_id(id_pokemon)
        capacites = [Attaque.from_id(capacite_id) for capacite_id in donnees["capacites"]]
        return Pokemon(
            id_pokemon,
            donnees["nom"],
            niveau,
            0,
            [Types.get_type(type_pokemon) for type_pokemon in donnees["types"]],
            donnees["stat_pv"],
            donnees["stat_att"],
            donnees["stat_defense"],
            donnees["stat_att_spe"],
            donnees["stat_def_spe"],
            donnees["stat_vit"],
            capacites,
            donnees["base_exp"]
        )

    @staticmethod
    def from_mix(donnees_additionnelle):
        donnees_theorique = Pokemon.DONNEES.get_donneees_avec_id(donnees_additionnelle["id"])
        capacites = [Attaque.from_id(capacite) if type(capacite) == int else Attaque.from_id(capacite["id"]) for capacite in donnees_additionnelle["capacites"]]
        return Pokemon(
            donnees_theorique["id"],
            donnees_theorique["nom"],
            donnees_additionnelle["niveau"],
            0,
            [Types.get_type(type_pokemon) for type_pokemon in donnees_theorique["types"]],
            donnees_theorique["stat_pv"],
            donnees_theorique["stat_att"],
            donnees_theorique["stat_defense"],
            donnees_theorique["stat_att_spe"],
            donnees_theorique["stat_def_spe"],
            donnees_theorique["stat_vit"],
            capacites,
            donnees_theorique["base_exp"],
            PV_actuel=donnees_additionnelle.get("PV_actuel", None)
        )
