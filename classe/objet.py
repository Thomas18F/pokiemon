import json
from abc import ABC
from typing import Union

from classe.attaque import Attaque


class ObjetDonnees:
    def __init__(self):
        with open("donnees/objet.json", "r") as fichier_lecture:
            self.donnees = json.loads(fichier_lecture.read())

    def get_donneees_avec_id(self, id_objet):
        for donnee in self.donnees:
            if donnee["id"] == id_objet:
                return donnee


class Objet(ABC):
    DONNEES = ObjetDonnees()

    def __init__(self, identifiant: int, texte: str):
        self.identifiant = identifiant
        self.texte = texte

    def serialise(self):
        return {"identifiant": self.identifiant}

    @staticmethod
    def from_id(identifiant: int):
        donnees = Objet.DONNEES.get_donneees_avec_id(identifiant)
        if donnees["categorie"] == "ball":
            return Ball(
                identifiant,
                "une " + donnees["nom"],
                donnees["taux"],
                donnees["prix"]
            )
        elif donnees["categorie"] == "CT":
            return CT(
                identifiant,
                "la CT" + str(donnees["id_ct"]),
                donnees["id_ct"]
            )


class Ball(Objet):
    def __init__(self, identifiant: int, texte: str, taux: Union[int, float], prix: int):
        super().__init__(identifiant, texte)
        self.taux = taux
        self.prix = prix
        self.nombre = 1

    def serialise(self):
        return {"identifiant": self.identifiant, "nombre": self.nombre}


class CT(Objet):
    def __init__(self, identifiant: int, texte: str, id_attaque: int):
        super().__init__(identifiant, texte)
        self.id_attaque: int = id_attaque
        self.attaque: Attaque = Attaque.from_id(id_attaque)
