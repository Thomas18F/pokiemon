import json
from os import path

import pygame

from parametre import Ecran


class CollectionSprites:
    def __init__(self, fichier: str):
        self.__sprites: dict = {}
        self.__classe = "bas"
        self.__indice = 0

        images_joueur = pygame.image.load(path.join("img", "assets", fichier + ".png")).convert_alpha()
        with open('img/assets/red.json', 'r') as fichier_lecture:
            separateur_images = json.load(fichier_lecture)

        for donnee_image in separateur_images["cadres"]:
            dimensions = (donnee_image['x'], donnee_image['y'], donnee_image['width'], donnee_image['height'])

            frame = pygame.surface.Surface((donnee_image['width'], donnee_image['height']))

            frame.blit(images_joueur, (0, 0), dimensions)
            frame = pygame.transform.scale(frame, (Ecran.PERSONNAGE_TAILLE_X, Ecran.PERSONNAGE_TAILLE_Y))

            liste_sprites: list = self.__sprites.get(donnee_image["name"], [])
            liste_sprites.append(frame)
            self.__sprites[donnee_image["name"]] = liste_sprites

    def __sprite(self, classe: str, indice: int):
        return self.__sprites[classe][indice]

    def get(self):
        return self.__sprite(self.__classe, self.__indice)

    def nouveau(self, classe: str):
        self.__classe = classe
        self.__indice = 0

    def suivant(self):
        self.__indice = (self.__indice + 1) % len(self.__sprites[self.__classe])
