
class GestionValeur:
    def __init__(self, valeurs):
        self.valeurs = valeurs

    def serialise(self):
        return self.valeurs

    def est_vrai(self, carte_nom: str, clef_valeur: str):
        if carte_nom in self.valeurs and clef_valeur in self.valeurs[carte_nom] and self.valeurs[carte_nom][clef_valeur]:
            return True
        return False

    def ajoute_valeur(self, carte_nom: str, clef_valeur: str, valeur: bool = True):
        if carte_nom in self.valeurs:
            self.valeurs[carte_nom][clef_valeur] = valeur
        else:
            self.valeurs[carte_nom] = {clef_valeur: valeur}
