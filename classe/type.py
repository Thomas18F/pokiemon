class Affinite:
    INSENSIBLE = 0
    PEU_EFFICACE = 0.5
    NEUTRE = 1
    SUPER_EFFICACE = 2


class Type:
    def __init__(self, indice, nom):
        self.indice = indice
        self.nom = nom

    def serialise(self):
        return self.nom


class Types:
    NORMAL = Type(0, "Normal")
    PLANTE = Type(1, "Plante")
    FEU = Type(2, "Feu")
    EAU = Type(3, "Eau")
    ELECTRIQUE = Type(4, "Electrique")
    GLACE = Type(5, "Glace")
    COMBAT = Type(6, "Combat")
    POISON = Type(7, "Poison")
    SOL = Type(8, "Sol")
    VOL = Type(9, "Vol")
    PSY = Type(10, "Psy")
    INSECTE = Type(11, "Insecte")
    ROCHE = Type(12, "Roche")
    SPECTRE = Type(13, "Spectre")
    DRAGON = Type(14, "Dragon")
    TENEBRE = Type(15, "Tenebre")
    ACIER = Type(16, "Acier")
    FEE = Type(17, "Fee")

    __LISTE_TYPES = (NORMAL, PLANTE, FEU, EAU, ELECTRIQUE, GLACE, COMBAT, POISON, SOL, VOL, PSY, INSECTE, ROCHE, SPECTRE, DRAGON, TENEBRE, ACIER, FEE)

    __AFFINITES = (
        (Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.PEU_EFFICACE, Affinite.INSENSIBLE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE,
         Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE,
         Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.INSENSIBLE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE),
        (Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE,
         Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE,
         Affinite.SUPER_EFFICACE, Affinite.INSENSIBLE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE),
        (Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.INSENSIBLE, Affinite.SUPER_EFFICACE),
        (Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.INSENSIBLE, Affinite.NEUTRE, Affinite.PEU_EFFICACE,
         Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE,
         Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE,
         Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.SUPER_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.INSENSIBLE, Affinite.PEU_EFFICACE, Affinite.NEUTRE),
        (Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE),
        (Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE,
         Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE),
        (Affinite.INSENSIBLE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE),
        (Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.INSENSIBLE),
        (Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE),
        (Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.SUPER_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.SUPER_EFFICACE),
        (Affinite.NEUTRE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE, Affinite.NEUTRE,
         Affinite.NEUTRE, Affinite.NEUTRE, Affinite.SUPER_EFFICACE, Affinite.SUPER_EFFICACE, Affinite.PEU_EFFICACE, Affinite.NEUTRE)
    )

    @staticmethod
    def affinite_simple(type_attaque: Type, type_defense: Type):
        return Types.__AFFINITES[type_attaque.indice][type_defense.indice]

    @staticmethod
    def get_type(type_chaine: str):
        for type_pokemon in Types.__LISTE_TYPES:
            if type_pokemon.nom == type_chaine:
                return type_pokemon
